new Vue({
    el: '#app',
    data () {
      return {
        characters: null,
        episode: null
      }
    },
   
    methods:{
        getCharacters(){
            axios
                .get('https://rickandmortyapi.com/api/character')
                    .then(response=>{
                        this.characters = response.data.results
                    })

                    .catch( e=> console.log(e))
        },

        getEpisodes(){
            axios
                .get('https://rickandmortyapi.com/api/episode')
                    .then(response=>{
                        this.episode = response.data.results
                    })

                    .catch( e=> console.log(e))
        },
    
    }, mounted () {
      this.getCharacters();
      this.getEpisodes();
      

    },
  })